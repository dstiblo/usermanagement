<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Squad;
use AppBundle\Form\SquadType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SquadRegistrationController extends Controller
{
    /**
     * @Route("/squadRegister", name="squadRegister")
     */
    public function squadRegisterAction(Request $request)
    {
        // Create a new blank user and process the form
        $group = new Squad();
        $form = $this->createForm(SquadType::class, $group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            // Save
            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
            $em->flush();

            return $this->redirectToRoute('login');
        }

        return $this->render('squad/squadRegister.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}